#!/bin/bash

echo "LAMP Installer with WordPress."

while true; do

read -p "Do you want to proceed installing LAMP/WordPress? (y/n) " yn

case $yn in 
	[yY] ) echo Okay, proceeding to install LAMP/WordPress!;
		break;;
	[nN] ) echo Okay, not installing LAMP/WordPress!;
		exit;;
	* ) echo 'Sorry, that is an invalid response!';;
esac

done


#Set user defined variables

read -p "What's your e-mail address Example would be redprezstuff@gmail.com : " adminmail

read -p "Please enter your domain name including your TLD. Example would be redprez16.com :" domainname

read -p "Please enter a database name for a MySQL database. :" databasename

read -p "Please enter a username for the MySQL database :" usernamedb

read -p "Please enter a password for the MySQL database :" dbpassword

#Delete old .lampvars and set new variables
rm .lampvars
cat <<EOT >> .lampvars
export adminmail 
export domainname
export databasename
export usernamedb
export wwwdata=www-data
EOT

#Source .lampvars
. .lampvars

#Echo test to make sure all variables are set. 
echo $domainname
echo $adminmail
echo $databasename
echo $usernamedb
echo $dbpassword
echo $wwwdata
#Install required packages

echo "Installing LAMP stack. This is not the full installation. Just the packages"


sudo add-apt-repository ppa:ondrej/php 
sudo apt update
sudo apt install apache2 mysql-server php8.3 php8.3-mysql phpmyadmin p7zip-full php8.3-cgi php8.3-mysqli php-pear php8.3-mbstring libapache2-mod-php php8.3-common php-phpseclib php8.3-mysql -y

echo "Setting up part. Please follow and answer all the prompts."

#Set up the MySQL Database
sudo mysql -u root -e "create database $databasename"; 

sudo mysql -u root -e "CREATE USER '$usernamedb'@'localhost' IDENTIFIED BY '$dbpassword'";

sudo mysql -u root -e "GRANT ALL PRIVILEGES ON *.* to '$usernamedb'@'localhost' WITH GRANT OPTION";

sudo mysql -u root -e "FLUSH PRIVILEGES";

#Stuff related to /var/www

sudo rm -rf /var/www
sudo mkdir /var/www
sudo mkdir /var/www/$domainname
sudo chmod -R 755 /var/www
sudo chown -R $wwwdata:$wwwdata /var/www/ 


ls -l /var/www/

#Append required lines to $domainname.conf file

sudo bash -c "cat <<EOT >> /etc/apache2/sites-available/$domainname.conf
<VirtualHost *:80>
ServerName $domainname
ServerAlias www.$domainname
DocumentRoot /var/www/$domainname
<Directory /var/www/$domainname>
Options Indexes FollowSymLinks MultiViews
AllowOverride All
Order allow,deny
Allow from all
Require all granted
</Directory>
</VirtualHost>
EOT"

#Download and extract WordPress to /var/www/$domainname

sudo wget https://wordpress.org/latest.zip -P /var/www/$domainname/latest.zip

echo "Expanding WordPress files from .ZIP file"

sudo 7za x -y  /var/www/$domainname/latest.zip/ -o/var/www/$domainname

sudo cp -r /var/www/$domainname/wordpress/* /var/www/$domainname

#Apache2 final steps. Reload Apache2 after enabling .conf file.

sudo a2ensite $domainname.conf
sudo a2dissite 000-default.conf
sudo apache2ctl configtest
sudo systemctl restart apache2


#Workaround for now. CHOWN the /var/www/$domainname folder after downloading and extracting WordPress.

sudo chown -R $wwwdata:$wwwdata /var/www/$domainname/
ls -l /var/www/$domainname

#Done!

echo "Done! You should be able to access your webserver at $domainname! Enjoy!"
