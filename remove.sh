#!/bin/bash

while true; do

read -p "Do you want to proceed? (y/n) " yn

case $yn in 
	[yY] ) echo Okay, proceeding to remove everything related to LAMP!;
		break;;
	[nN] ) echo Okay, not removing anything related to LAMP!;
		exit;;
	* ) echo 'Sorry, that is an invalid response!';;
esac

done

echo "Removing everything related to LAMP now!" 

sudo apt purge -y apache2* mysql-server php8.3 php8.3-mysql phpmyadmin p7zip-full 
sudo add-apt-repository -r ppa:ondrej/php -y

sudo rm -rf /var/www/*

sudo rm -rf /etc/apache2
